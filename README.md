# Gitlab CI/CD - Project Variables

Set many variables for different environments in a GitLab project could be a boring task and not devoid of human errors.  
For this reason I decided to create a bash script to manage GitLab project variables in smart way through a JSON file, mainly based on the environment name and keys/values for variable definitions, easy to read and manage.

The verbosity of the script is high, you will see what script is doing and the results (JSON) of each request to the GitLab APIs.

Currently you can do two things: 
* read variables
* set variables defined in the JSON file

# Getting Started

You just need a standard *bash* terminal and the `jq` package installed on your machine.  
I didn't include a validation for `jq` binary, please [check documentation](https://github.com/stedolan/jq) for your system installation.

Check the version and go ahead:
```bash
$ jq --version
jq-1.6
```

## The JSON file
In this repo I included an example of the JSON file required to update GitLab variables.  
Each JSON level starts with the environment name as main key and then the list of key/value pairs for variables definition.

There isn't any option to set other variable properties such as *masked* or *protected*. That's it.

In the following example of the `variables.json` file you can see how to **set three variables** in the GitLab environment `project-a-prod`:

```
{
  "envs": {
    "project-a-prod": {
      "NAMESPACE": "project-a-prod-ns",
      "DOCKER_IMAGE": "python:3.7",
      "LIB_REF": "0.1"
    },
    [...]
  }
}
```

## Run
When you are done with variables definition in the JSON file you can run the script and input required values to manage authentication on GitLab for reading/writing variables through API.  
If you have to repeat the execution of the script several times, you can set the required configuration value in the environment variables.

Inputs required during script execution or set as env variable (the name of var in the brackets):
* GitLab URL of your project (`GITLAB_URL`)  
  The default value is `https://gitlab.com`
* The project ID (`GITLAB_PROJECT_ID`)  
  *Project* > *Settings* > *General* > *Project ID*
* A GitLab Token (`GITLAB_TOKEN`)  
  You must have [an enabled GitLab Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) in order to complete all read/write operations. 

Avoid runtime inputs with env variables definition in your local system:

```bash
export GITLAB_URL=https://example.gitlab.com
export GITLAB_PROJECT_ID=12345678
export GITLAB_TOKEN=api-token
```

A custom JSON variables file can be set with:

```bash
export VARIABLES_JSON_FILE=/my/custom/vars.json
```

*Tips*: Create a `.env` file (ignored from git) with the definition of required variables as showed above and load it before running the script.

```bash
source .env
./gitlab-vars.sh
```
 
# How it works
The bash script requires a first argument as command (`show`, `set`) and, if necessary, some arguments.
Run the command without arguments to display the help.

```
./gitlab-vars.sh

Usage:
./gitlab-vars.sh <command> <arguments>

Available commands:
- set (set all variables included in the variables.json file)
- show (list all variables available in the project)
- show <variable name> <env name> (show variable details from a specific environment)
```

The `set` command will start the import of the whole JSON file, double check of the variables definition before starting, there is no confirmation.  
* If a variables doesn't exist, it's created and then updated with the real value.
* If a variable value is the same of the definition, the updated is skipped.

For reading you have two options, you can list all current available variables in the project or display details of one variable in a environment. For this task you have to use `show` command.

# Screenshots

Set variables for the first time (variables are missing)

![gitlab-cicd-project-variables set1](https://storage.googleapis.com/particles-public/projects/gitlab/gitlab-cicd-project-variables/gitlab-vars-set1.png)

Set again when variables already exists (skip)

![gitlab-cicd-project-variables set2](https://storage.googleapis.com/particles-public/projects/gitlab/gitlab-cicd-project-variables/gitlab-vars-set2.png)

Show the list of all GitLab variables

![gitlab-cicd-project-variables show](https://storage.googleapis.com/particles-public/projects/gitlab/gitlab-cicd-project-variables/gitlab-vars-show.png)


# License
MIT

# Author Information
Fabio Ferrari - Cloud Architect and DevOps Engineer   
https://particles.io








