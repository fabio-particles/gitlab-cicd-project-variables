#!/usr/bin/env bash
# Skip runtime configuration with env variables setting:
# export GITLAB_URL=https://example.gitlab.com
# export GITLAB_PROJECT_ID=12345678
# export GITLAB_TOKEN=api-token
#
# Custom variables JSON file
# export VARIABLES_JSON_FILE=/my/custom/vars.json

VARIABLES_JSON_FILE="${VARIABLES_JSON_FILE:-variables.json}"
CMD=$1
CMD_ARG1=$2
CMD_ARG2=$3

# Help
help () {
  echo "Usage:"
  echo "$0 <command> <arguments>"
  echo
  echo "Available commands:"
  echo "- set (set all variables included in the $VARIABLES_JSON_FILE file)"
  echo "- show (list all variables available in the project)"
  echo "- show <variable name> <env name> (show variable details from a specific environment)"
}

# Display enhanced messages
enhanced_msg () {
  case $1 in
    cmd_missing)
      printf "\e[0;31m\xE2\x98\xA0 Command is missing\e[m\n"
      ;;
    permissions_failed)
      printf "\e[0;31m\xE2\x98\xA0 Permissions are insufficient\e[m\n"
      ;;
    permissions_ok)
      printf "\e[0;32m\xE2\x9c\x93 Ok, let's do it\e[m\n"
      ;;
    create_variable)
      printf "\e[0;33m\xE2\x98\x90 Create variable $2\e[m\n"
      ;;
    set_variable)
      printf "\e[0;32m\xE2\x9c\x93 Update variable $2\e[m\n"
      ;;
    skip_update)
      printf "Update required:\e[0;32m NO (skip)\e[m\n"
      ;;
    go_update)
      printf "Update required:\e[0;33m YES \e[m\n"
      ;;
    var_exists)
      printf "\e[0;33m\xE2\x9c\x93 Variables $2 exists\e[m\n"
      ;;
    *)
      printf "\e[0;31m\xE2\x98\xA0 Unknow error\e[m\n"
      ;;
  esac
} 

# Check CMD argument
[ -z $CMD ] && enhanced_msg "cmd_missing" && help && exit 1

# Input the GitLab URL
if [ -z $GITLAB_URL ]; then
  read -p "GitLab URL [https://gitlab.com]: "
  GITLAB_URL="${REPLY:-https://gitlab.com}"
fi

# Input the GitLab Project ID
if [ -z $GITLAB_PROJECT_ID ]; then
  read -p "GitLab Project ID: "
  GITLAB_PROJECT_ID=$REPLY
fi

# Input the GitLab Token
if [ -z $GITLAB_TOKEN ]; then
  read -p "GitLab Token: "
  GITLAB_TOKEN=$REPLY
fi

# Functions
json_display () {
  if command -v jq &> /dev/null; then echo $curl_out | jq; else echo $curl_out; fi; echo
}

curl_get () {
  # echo curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/projects/$GITLAB_PROJECT_ID/$1"
  curl_out=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/projects/$GITLAB_PROJECT_ID/$1")
}

show_var () {
  local key=$1
  local env=$2
  if [ ! -z $key ]; then local get_var="/$1"; fi
  if [ ! -z $env ]; then local get_env="filter\[environment_scope\]=$env"; fi
  echo "- GitLab variables:"
  curl_get "variables$get_var?$get_env"
  json_display
}

var_exists () {
  local env=$1
  local key=$2
  local value=$3
  curl_get "variables/$key?filter\[environment_scope\]=$env"
  local var_key=$(jq -r ".key" <<< $curl_out)
  local var_value=$(jq -r ".value" <<< $curl_out)
  if [ "$var_key" == "$key" ]; then
    enhanced_msg "var_exists" $key
    if [ "$var_value" == "$value" ]; then
      enhanced_msg "skip_update"
      var_skip_update="yes"
    else
      enhanced_msg "go_update"
      var_skip_update="no"
    fi
  else
    # echo "- variable $key exists: NO"
    if [ "$var_skip_update" != "yes" ]; then
      enhanced_msg "create_variable" $var
      curl_out=$(curl -s --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
        "$GITLAB_URL/api/v4/projects/$GITLAB_PROJECT_ID/variables" \
        --form "key=$key" \
        --form "value=null" \
        --form environment_scope=$env)
      json_display
      var_skip_update="no"
    fi
  fi
}

set_vars () {
  for env in $(jq -r '.envs | keys[]' $VARIABLES_JSON_FILE); do
    echo "# Set variables in $env environment"
    jnode=".envs.\"$env\""
    env_variables=$(jq "$jnode" $VARIABLES_JSON_FILE)
    for var in $(echo $env_variables | jq -r "keys[]"); do
      value=$(jq -r "$jnode.\"$var\"" $VARIABLES_JSON_FILE)
      # Check if variable exists
      var_exists $env $var $value
      if [ "$var_skip_update" == "no" ]; then
        enhanced_msg "set_variable" $var
        curl_out=$(curl -s --request PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
          "$GITLAB_URL/api/v4/projects/$GITLAB_PROJECT_ID/variables/$var?filter\[environment_scope\]=$env" \
          --form "value=$value")
        json_display
      fi
    done
  done
}

# Check GitLab permissions (>= 40)
# Available roles (weight):
# - No access (0)
# - Minimal access (5) (Introduced in GitLab 13.5.)
# - Guest (10)
# - Reporter (20)
# - Developer (30)
# - Maintainer (40)
echo "# Check GitLab permissions on project ID $GITLAB_PROJECT_ID"
curl_get 
permissions_level=$(jq ".permissions.project_access.access_level" <<< $curl_out)
group_permissions_level="$(jq ".permissions.group_access.access_level" <<< $curl_out)"
if [ $permissions_level -ge 40 ] || [ $group_permissions_level -ge 40 ]; then
  enhanced_msg "permissions_ok"
else
  enhanced_msg "permissions_failed"
  exit 1 
fi

# Init
case $CMD in
  show)
    show_var $CMD_ARG1 $CMD_ARG2 
    ;;
  set)
    set_vars
    ;;
esac